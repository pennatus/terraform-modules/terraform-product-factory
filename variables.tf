/********************************************************
 * Required
 ********************************************************/

variable "MONITORING_PROJECT_ID" {
  description = "ID of project that is hooked to stack monitoring services - usually the is-org project id."
}

variable "PRODUCT_NAME" {
  description = "This is the top level name of the product - i.e. website"
  type = string
}

variable "BILLING_DISPLAY_NAME" {
  description = "This is the friendly display name of the billing account"
  type = string
}

variable "DOMAIN" {
  description = "This is domain of the organization"
  type = string
}

variable "FOLDER_IDS" {
  description = "Map of folder names to folder ids (folders/folder_id)"
  type = map
}

/********************************************************
 * Optional
 ********************************************************/

variable "ENVIRONMENTS" {
  description = "List of environment that apply for this product"
  type = list
  default = [ "dev", "staging", "prod" ]
}

variable "APIS" {
  description = "List of apis to enable"
  type = list
  default = [ "compute", "run" ]
}

variable "SSH_KEYS" {
  description = "List of public ssh keys to add to the project so that VMs automatically use. Example 'jsmith: ssh-rsa AAAAB3Nza****HlGKiR5dP2IJ5Guw== jsmith@myorg.com'"
  type = list
  default = [ ]
}

variable "KV_SECRET_PATHS" {
  description = "List of kv-2 paths that need to be created for this product.  Paths will be created at: product/[env]/[productname]/kv-v2/[path]"
  type = list
  default = [ ]
}