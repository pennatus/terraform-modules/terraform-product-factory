# product-factory

Creates a new Product which includes Vault policies, GCP Roles, GCP projects and optional items for each environment.

## Usage

```hcl
module "product_website" {
  source = "git::https://gitlab.com/pennatus/infrastructure//modules/product-factory"

  PRODUCT_NAME = "website"
  ENVIRONMENTS = [ "dev", "prod" ]
  APIS = [ ]
  SSH_KEYS = [ "jsmith: ssh-rsa AAAAB3NzaC1yc2E*****== jsmith@example.com" ]
  KV_SECRET_PATHS = [ "database" ]

  BILLING_DISPLAY_NAME = var.BILLING_NAME_GENERAL
  DOMAIN = var.DOMAIN
  FOLDER_IDS = module.org_folders.folder_ids
}
output project_ids {
  value = module.product_website.env_project_map
}
```


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| BILLING\_DISPLAY\_NAME | This is the friendly display name of the billing account | string | n/a | yes |
| DOMAIN | This is domain of the organization | string | n/a | yes |
| FOLDER\_IDS | Map of folder names to folder ids \(folders/folder\_id\) | map | n/a | yes |
| KEYS\_PROJECT\_ID | Project Id of the GCP project that holds the storage buckets containing sensitive keys for each product | string | n/a | yes |
| PRODUCT\_NAME | This is the top level name of the product - i.e. website | string | n/a | yes |
| APIS | List of apis to enable | list | `[ "compute", "run" ]` | no |
| ENVIRONMENTS | List of environment that apply for this product | list | `[ "dev", "staging", "prod" ]` | no |
| KV\_SECRET\_PATHS | List of kv-2 paths that need to be created for this product | list | `[]` | no |
| SSH\_KEYS | List of public ssh keys to add to the project so that VMs automatically use | list | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| env\_project\_map |  |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

