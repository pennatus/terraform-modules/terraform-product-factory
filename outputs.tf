output "env_project_map" {
  value = local.env_project_map
  description = "Map of environment to project ids.  For instance { prd: { id = prd-myproject-abcd, number = 12345 }}"
}

output "vault_viewer_policies" {
  value = vault_policy.viewer
  description = "Vault policies created with viewer access to all project resources"
}

output "vault_editor_policies" {
  value = vault_policy.editor
  description = "Vault policies created with editor access to all project resources"
}

output "vault_owner_policies" {
  value = vault_policy.owner
  description = "Vault policies created with owner access to all project resources"
}