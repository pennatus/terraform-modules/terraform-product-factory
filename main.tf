data "google_organization" "org" {
  domain = var.DOMAIN
}

data "google_billing_account" "acct" {
  display_name = var.BILLING_DISPLAY_NAME
}

locals {
  env_project_map = { for env in var.ENVIRONMENTS :
    env => {
      id = google_project.project[env].project_id
      number = google_project.project[env].number
    }
  }
}

resource "random_string" "project_id_suffix" {
  for_each = toset(var.ENVIRONMENTS)

  length = 22
  upper = false
  special = false
}

resource "google_project" "project" {
  for_each = toset(var.ENVIRONMENTS)

  name = "${each.key}-${lower(var.PRODUCT_NAME)}"
  project_id = "${substr(each.key, 0, 3)}-${lower(substr(var.PRODUCT_NAME, 0, 20))}-${substr(random_string.project_id_suffix[each.key].result, 0, 5)}"
  folder_id  = var.FOLDER_IDS[each.key]
  billing_account = data.google_billing_account.acct.id
}

resource "google_project_service" "iamapi" {
  for_each = toset(var.ENVIRONMENTS)

  project = google_project.project[each.key].project_id
  service = "iam.googleapis.com"
}

resource "google_project_service" "compute" {
  for_each = toset(var.ENVIRONMENTS)

  project = google_project.project[each.key].project_id
  service = "compute.googleapis.com"
}

resource "google_project_service" "serviceusageapi" {
  for_each = toset(var.ENVIRONMENTS)

  project = google_project.project[each.key].project_id
  service = "serviceusage.googleapis.com"
}

resource "google_project_service" "cloudresourcemanagerapi" {
  for_each = toset(var.ENVIRONMENTS)

  project = google_project.project[each.key].project_id
  service = "cloudresourcemanager.googleapis.com"
}

resource "google_project_service" "cloudsqladminapi" {
  for_each = toset(var.ENVIRONMENTS)

  project = google_project.project[each.key].project_id
  service = "sqladmin.googleapis.com"
}

resource "google_project_service" "api" {
  for_each = {
    for pair in setproduct(var.ENVIRONMENTS, var.APIS) : "${pair[0]}:${pair[1]}" => {
      env = pair[0]
      api = pair[1]
    }
  }

  project = local.env_project_map[each.value.env].id
  service = "${each.value.api}"
}

resource "google_compute_project_metadata" "ssh_key" {
  for_each = {
    for pair in setproduct(var.ENVIRONMENTS, var.SSH_KEYS) : "${pair[0]}:${pair[1]}" => {
      env = pair[0]
      ssh_key = pair[1]
    }
  }

  project = local.env_project_map[each.value.env].id
  metadata = {
      ssh-keys = each.value.ssh_key
  }
  depends_on = [ google_project.project, google_project_service.compute ]
}

resource "google_project_iam_member" "organization_member_roles" {
  for_each = toset(var.ENVIRONMENTS)

  project  = google_project.project[each.key].project_id
  role    = "roles/iam.serviceAccountTokenCreator"

  member = "serviceAccount:${google_project.project[each.key].number}-compute@developer.gserviceaccount.com"

  depends_on = [ google_project_service.compute ]
}

resource "google_organization_iam_member" "organization_member_roles" {
  for_each = toset(var.ENVIRONMENTS)

  org_id  = data.google_organization.org.org_id
  role    = "roles/cloudsql.client"

  member = "serviceAccount:${google_project.project[each.key].number}-compute@developer.gserviceaccount.com"

  depends_on = [ google_project_service.compute ]
}


resource "vault_mount" "prod_secret_kv_2" {
  for_each = {
    for pair in setproduct(var.ENVIRONMENTS, var.KV_SECRET_PATHS) : "${pair[0]}:${pair[1]}" => {
      env = pair[0]
      kv_path = pair[1]
    }
  }

  path        = "product/${each.value.env}/${lower(var.PRODUCT_NAME)}/kv-v2/${each.value.kv_path}"
  type        = "kv-v2"
}

resource "vault_gcp_secret_roleset" "gcp_backend_secret_owner" {
  for_each      = toset(var.ENVIRONMENTS)

  backend      = "/org/gcp"
  roleset      = "${substr(each.key, 0, 3)}-${substr(lower(var.PRODUCT_NAME), 0, 7)}-ow"
  secret_type  = "access_token"
  project      = google_project.project[each.key].project_id
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${google_project.project[each.key].project_id}"

    roles = [
      "roles/owner",
    ]
  }

  binding {
    resource = "//cloudresourcemanager.googleapis.com/organizations/${data.google_organization.org.org_id}"

    roles = [
      "roles/cloudsql.client",
    ]
  }

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${var.MONITORING_PROJECT_ID}"

    roles = [
      "roles/monitoring.notificationChannelEditor",
      "roles/monitoring.alertPolicyEditor",
      "roles/monitoring.uptimeCheckConfigEditor",
    ]
  }

  depends_on = [ google_project_service.iamapi, google_project_service.compute ]
}

resource "vault_gcp_secret_roleset" "gcp_backend_secret_service_account" {
  for_each      = toset(var.ENVIRONMENTS)

  backend      = "/org/gcp"
  roleset      = "${substr(each.key, 0, 3)}-${substr(lower(var.PRODUCT_NAME), 0, 7)}-sa"
  secret_type  = "service_account_key"
  project      = google_project.project[each.key].project_id
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${google_project.project[each.key].project_id}"

    roles = [
      "roles/owner",
    ]
  }

  binding {
    resource = "//cloudresourcemanager.googleapis.com/organizations/${data.google_organization.org.org_id}"

    roles = [
      "roles/cloudsql.client",
    ]
  }

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${var.MONITORING_PROJECT_ID}"

    roles = [
      "roles/monitoring.notificationChannelEditor",
      "roles/monitoring.alertPolicyEditor",
      "roles/monitoring.uptimeCheckConfigEditor",
    ]
  }

  depends_on = [ google_project_service.iamapi, google_project_service.compute ]
}

resource "vault_gcp_secret_roleset" "gcp_backend_secret_editor" {
  for_each      = toset(var.ENVIRONMENTS)

  backend      = "/org/gcp"
  roleset      = "${substr(each.key, 0, 3)}-${substr(lower(var.PRODUCT_NAME), 0, 7)}-ed"
  secret_type  = "access_token"
  project      = google_project.project[each.key].project_id
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${google_project.project[each.key].project_id}"

    roles = [
      "roles/editor",
    ]
  }

  depends_on = [ google_project_service.iamapi, google_project_service.compute ]
}

resource "vault_gcp_secret_roleset" "gcp_backend_secret_viewer" {
  for_each      = toset(var.ENVIRONMENTS)

  backend      = "/org/gcp"
  roleset      = "${substr(each.key, 0, 3)}-${substr(lower(var.PRODUCT_NAME), 0, 7)}-vi"
  secret_type  = "access_token"
  project      = google_project.project[each.key].project_id
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${google_project.project[each.key].project_id}"

    roles = [
      "roles/viewer",
    ]
  }

  depends_on = [ google_project_service.iamapi, google_project_service.compute ]
}


data "vault_policy_document" "policy_doc_viewer" {
  for_each        = toset(var.ENVIRONMENTS)

  rule {
    path         = "/product/${each.key}/${lower(var.PRODUCT_NAME)}/*"
    capabilities = ["read"]
  }
  rule {
    path         = "/org/gcp/token/${substr(each.key, 0, 3)}-${substr(lower(var.PRODUCT_NAME), 0, 7)}-vi"
    capabilities = ["read"]
  }
}

data "vault_policy_document" "policy_doc_editor" {
  for_each        = toset(var.ENVIRONMENTS)

  rule {
    path         = "/product/${each.key}/${lower(var.PRODUCT_NAME)}/*"
    capabilities = ["read", "update", "list"]
  }
  rule {
    path         = "/org/gcp/token/${substr(each.key, 0, 3)}-${substr(lower(var.PRODUCT_NAME), 0, 7)}-vi"
    capabilities = ["read"]
  }
  rule {
    path         = "/org/gcp/token/${substr(each.key, 0, 3)}-${substr(lower(var.PRODUCT_NAME), 0, 7)}-ed"
    capabilities = ["read"]
  }
}

data "vault_policy_document" "policy_doc_owner" {
  for_each        = toset(var.ENVIRONMENTS)

  rule {
    path         = "/product/${each.key}/${lower(var.PRODUCT_NAME)}/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    path         = "/org/gcp/token/${substr(each.key, 0, 3)}-${substr(lower(var.PRODUCT_NAME), 0, 7)}-*"
    capabilities = ["read"]
  }
  rule {
    path         = "/org/gcp/key/${substr(each.key, 0, 3)}-${substr(lower(var.PRODUCT_NAME), 0, 7)}-*"
    capabilities = ["read"]
  }
}

resource "vault_policy" "viewer" {
  for_each = toset(var.ENVIRONMENTS)

  name    = "product-${each.key}-${lower(var.PRODUCT_NAME)}-viewer"
  policy  = data.vault_policy_document.policy_doc_viewer[each.key].hcl
}

resource "vault_policy" "editor" {
  for_each = toset(var.ENVIRONMENTS)

  name   = "product-${each.key}-${lower(var.PRODUCT_NAME)}-editor"
  policy = data.vault_policy_document.policy_doc_editor[each.key].hcl
}

resource "vault_policy" "owner" {
  for_each = toset(var.ENVIRONMENTS)

  name   = "product-${each.key}-${lower(var.PRODUCT_NAME)}-owner"
  policy = data.vault_policy_document.policy_doc_owner[each.key].hcl
}

# resource "vault_gcp_auth_backend_role" "gcp" {
#   for_each                = toset(var.ENVIRONMENTS)

#   role                   = "product-${each.key}-${var.PRODUCT_NAME}-viewer"
#   type                   = "iam"
#   backend                = "gcp"
#   bound_projects         = [ google_project.project[each.key].project_id ]
#   bound_service_accounts = ["${google_project.project[each.key].number}-compute@developer.gserviceaccount.com"]
#   token_policies         = ["product-${var.ENVIRONMENTS[each.key]}-${var.PRODUCT_NAME}-viewer"]
# }
